import React from 'react';
import Layout from "@fnet/react-layout-asya";

import Rating from '@mui/material/Rating';

export default ({ input, ...props } = {}) => {

  const forms = props?.forms || {};

  const [inputValue, setInputValue] = React.useState(input?.value || 0);

  React.useEffect(() => {
    forms.setInputValue = setInputValue;
  }, []);

  return (
    <Layout {...props} title="Rating" description="Rating component from Material-UI">
      <Rating
        defaultValue={inputValue}
        precision={input?.precision || 0.5}
        onChange={input?.onChange}
        sx={{ fontSize: "2.5rem" }} />
    </Layout>
  );
}